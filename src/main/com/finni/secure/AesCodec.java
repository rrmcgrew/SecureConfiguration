package com.finni.secure;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AesCodec {
	private static final String ENC_ALG = "AES";
	private static final String ENC_ALG_FULL = "AES/CBC/PKCS5Padding";
	private static final int KEY_LENGTH = 16;
		
	private Key secret;
	
	public AesCodec(String key) throws GeneralSecurityException {
		this(key.getBytes());
	}
	
	public AesCodec(byte[] key) {
		if (key == null || key.length != KEY_LENGTH) {
			throw new IllegalArgumentException(String.format("key must be %d characters", KEY_LENGTH));
		} 
		
		secret = new SecretKeySpec(key, ENC_ALG);
	}
	
	public byte[] encrypt(String plain) throws GeneralSecurityException {
		return encrypt(plain.getBytes(), null);
	}
	
	public byte[] encrypt(byte[] plain, byte[] iv) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ENC_ALG_FULL);
		byte[] encrypted;
		
		if (iv == null) {
			cipher.init(Cipher.ENCRYPT_MODE, secret, new SecureRandom());
		} else if (iv.length != 16) {
			throw new IllegalArgumentException("iv must be 16 bytes in length");
		} else {
			cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));
		}		
		
		encrypted = cipher.doFinal(plain);
		
		return concatIvAndEncrypted(cipher.getIV(), encrypted);
	}
	
	public byte[] decrypt(byte[] encrypted) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ENC_ALG_FULL);
		Object[] ivAndData = split(encrypted, KEY_LENGTH);
		byte[] iv = (byte[]) ivAndData[0];
		byte[] data = (byte[]) ivAndData[1];
		
		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
		
		return cipher.doFinal(data);
	}
	
	protected final byte[] concatIvAndEncrypted(byte[] iv, byte[] encrypted) {
		if (iv == null || encrypted == null) {
			throw new IllegalArgumentException("neither array can be null");
		}
		
		int ivLength = iv.length;
		int encLength = encrypted.length;
		byte[] result = new byte[ivLength + encLength];
		
		System.arraycopy(iv, 0, result, 0, ivLength);
		System.arraycopy(encrypted, 0, result, iv.length, encrypted.length);
		
		return result;
	}
	
	protected final Object[] split(byte[] combined, int splitIndex) {
		if (combined == null) {
			throw new IllegalArgumentException("array can not be null");
		} else if (splitIndex > combined.length) {
			throw new IllegalArgumentException(
					"split index must be 0 or greater and must be smaller than the length of the array");
		}
		
		Object[] results = new Object[2];
		byte[] first = new byte[splitIndex];
		byte[] second = new byte[combined.length - splitIndex];
		
		System.arraycopy(combined, 0, first, 0, first.length);
		System.arraycopy(combined, splitIndex, second, 0, second.length);
		
		results[0] = first;
		results[1] = second;
		
		return results;
	}
}
