package com.finni.secure;

import java.io.Serializable;

public interface Configuration extends Serializable {
	public void validate();
}
