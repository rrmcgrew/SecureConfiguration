package com.finni.secure;

import java.io.File;
import java.io.Serializable;

public class SecureConfig {
	private final Configuration config;
	private final File file;
	private final String key;
	
	private SecureConfig(Builder<?> builder) {
		this.config = builder.config;
		this.file   = builder.file;
		this.key    = builder.key;
	}
	
	public static class Builder<T extends Configuration> {
		private final T config;
		
		private File file  = null;
		private String key = null;
		
		public Builder(T config) {
			this.config = config;
		}
		
		public Builder<T> file(File file) {
			this.file = file;
			
			return this;
		}
		
		public Builder<T> key(String key) {
			this.key = key;
			
			return this;
		}
		
		public SecureConfig build() {
			return new SecureConfig(this);
		}
	}

	public Serializable getConfig() {
		return config;
	}

	public File getFile() {
		return file;
	}

	public String getKey() {
		return key;
	}
}
