package com.finni.secure;

import static org.junit.Assert.assertTrue;

import java.security.GeneralSecurityException;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AesCodecTests {
	private static final String PLAIN = "plain";
	private static final String HEX_KEY = "2b7e151628aed2a6abf7158809cf4f3c";
	private static final TestVector[] VECTORS = {
			new TestVector("000102030405060708090A0B0C0D0E0F".toLowerCase(),"6bc1bee22e409f96e93d7e117393172a",
					"7649abac8119b246cee98e9b12e9197d"),
			new TestVector("7649ABAC8119B246CEE98E9B12E9197D".toLowerCase(),"ae2d8a571e03ac9c9eb76fac45af8e51",
					"5086cb9b507219ee95db113a917678b2"),
			new TestVector("5086CB9B507219EE95DB113A917678B2".toLowerCase(),"30c81c46a35ce411e5fbc1191a0a52ef",
					"73bed6b8e3c1743b7116e69e22229516"),
			new TestVector("73BED6B8E3C1743B7116E69E22229516".toLowerCase(),"f69f2445df4f9b17ad2b417be66c3710",
					"3ff1caa1681fac09120eca307586e1a7")};

	AesCodec codec;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void init() throws GeneralSecurityException {
		byte[] hexKey = DatatypeConverter.parseHexBinary(HEX_KEY);
		codec = new AesCodec(hexKey);
	}
	
	@Test
	public void plainText() throws GeneralSecurityException {
		byte[] encrypted = codec.encrypt(PLAIN);
		
		assertTrue(new String(codec.decrypt(encrypted)).equals(PLAIN));
	}
	
	@Test
	public void testVectors() throws GeneralSecurityException {
		for (final TestVector vector : VECTORS) {
			byte[] iv = DatatypeConverter.parseHexBinary(vector.iv);
			byte[] test = DatatypeConverter.parseHexBinary(vector.vector);
			byte[] encrypted = codec.encrypt(test, iv);
			byte[] decrypted = codec.decrypt(encrypted);
			String expected = vector.iv + vector.expected;
			String encryptedHex = 
					DatatypeConverter.printHexBinary(encrypted).toLowerCase().substring(0, expected.length());
			
			assertTrue(expected.equalsIgnoreCase(encryptedHex));
			assertTrue(vector.vector.equalsIgnoreCase(DatatypeConverter.printHexBinary(decrypted).toLowerCase()));
		}
	}
	
	@Test
	public void concat() {
		String first = "FirstString";
		String second = "SecondString";
		byte[] concat = codec.concatIvAndEncrypted(first.getBytes(), second.getBytes());
		byte[] concatEmptyStart = codec.concatIvAndEncrypted("".getBytes(), second.getBytes());
		byte[] concatEmptyEnd = codec.concatIvAndEncrypted(first.getBytes(), "".getBytes());
		
		assertTrue(new String(concat).equalsIgnoreCase(first + second));
		assertTrue(new String(concatEmptyStart).equalsIgnoreCase(second));
		assertTrue(new String(concatEmptyEnd).equalsIgnoreCase(first));
		
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("neither array can be null");
		
		codec.concatIvAndEncrypted(null, second.getBytes());
		codec.concatIvAndEncrypted(first.getBytes(), null);
	}
	
	@Test
	public void split() {
		String first = "FirstString";
		String second = "SecondString";
		String combined = "FirstStringSecondString";
		Object[] split = codec.split(combined.getBytes(), first.length());
		byte[] fragment1 = (byte[]) split[0];
		byte[] fragment2 = (byte[]) split[1];
		
		assertTrue(Arrays.equals(first.getBytes(), fragment1));
		assertTrue(Arrays.equals(second.getBytes(), fragment2));
		
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("array can not be null");
		
		codec.split(null, first.length());
		
		thrown.expectMessage("split index must be 0 or greater and must be smaller than the length of the array");
		
		codec.split(combined.getBytes(), 100);
	}
	
	static class TestVector {
		String iv;
		String vector;
		String expected;
		
		TestVector(String iv, String vector, String expected) {
			this.iv = iv;
			this.vector = vector;
			this.expected = expected;
		}
	}
}
