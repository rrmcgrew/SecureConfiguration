package com.finni.secure;

import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.finni.secure.SecureConfig.Builder;

@RunWith(MockitoJUnitRunner.class)
public class SecureConfigTests {
	private static final String CONFIG_STR = "this is my config";
	private static final StringConfiguration CONFIG = new StringConfiguration();
	
	Builder<StringConfiguration> builder;
	
	@Before
	public void init() {
		CONFIG.validate();
		builder = new Builder<>(CONFIG);
	}
	
	@Test
	public void build() {
		SecureConfig config = builder.file(new File("C:/Test")).key("secret_key").build();
		StringConfiguration strConfig = (StringConfiguration) config.getConfig();
		
		assertThat(strConfig, isA(Configuration.class));
		assertThat(strConfig.getString(), equalTo(CONFIG_STR));
	}
	
	private static class StringConfiguration implements Configuration {
		private static final long serialVersionUID = -4855082728524280188L;
		private static String s;
		
		@Override
		public void validate() {
			if (s == null) {
				s = CONFIG_STR;
			}
		}
		
		public String getString() {
			return s;
		}
	}
}
